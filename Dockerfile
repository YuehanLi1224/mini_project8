# Use an official Rust image as a parent image
FROM rust:1.64 as builder

# Create a new shell project
RUN USER=root cargo new --bin mini
WORKDIR /mini_proj8

# Copy the Cargo.toml and Cargo.lock files and your source code to the new project
COPY ./Cargo.toml ./Cargo.lock ./
COPY ./src ./src

# Build your application
RUN cargo build --release

# Use the Debian buster image as a base for the final stage
FROM debian:buster-slim

# Copy the binary from the builder stage to the final image
COPY --from=builder /mini_proj8/target/release/mini_proj8 /usr/local/bin/mini_proj8

# Set the binary as the container's entrypoint
ENTRYPOINT ["mini_proj8"]
