Screenshot of the output from running my CLI tool:
![Alt text](screenshot1.jpg)

Screenshot of my successful Docker build and run commands for CI/CD:
![Alt text](screenshot2.jpg)

Screenshot of  successful test run:
![Alt text](screenshot3.jpg)