.PHONY: build test clean run

build:
	cargo build

test:
	cargo test

clean:
	cargo clean

run:
	cargo run

check:
	cargo check
	
release:
	cargo build --release
