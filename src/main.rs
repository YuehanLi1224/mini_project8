use clap::{App, Arg};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Default, Debug, PartialEq)]
struct ResData {
    method: String,
    result: i32,
}

// A simple function to sum two integers
fn sum(x1: i32, x2: i32) -> ResData {
    ResData {
        method: "sum".to_string(),
        result: x1 + x2,
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = App::new("Sum Calculator")
        .version("1.0")
        .about("Sums two integers")
        .arg(Arg::with_name("x1")
             .short('a')
             .long("value1")
             .takes_value(true)
             .required(true)
             .help("First integer"))
        .arg(Arg::with_name("x2")
             .short('b')
             .long("value2")
             .takes_value(true)
             .required(true)
             .help("Second integer"))
        .get_matches();

    let x1: i32 = matches.value_of("x1").unwrap().parse()?;
    let x2: i32 = matches.value_of("x2").unwrap().parse()?;

    let result = sum(x1, x2);

    println!("The sum of {} and {} is {}", x1, x2, result.result);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sum() {
        let result = sum(2, 3);
        assert_eq!(result, ResData { method: "sum".to_string(), result: 5 });
    }
}
