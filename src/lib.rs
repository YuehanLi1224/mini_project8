pub fn add(a: i32, b: i32) -> i32 {
    a + b
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add() {
        assert_eq!(add(1, 2), 3);
    }
    
    #[test]
    #[should_panic]
    fn test_add_failure() {
        assert_eq!(add(1, 2), 4);
    } 

    #[test]
    #[ignore]
    fn expensive_test() {

    }
}
